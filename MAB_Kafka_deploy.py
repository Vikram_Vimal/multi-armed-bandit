import json
import sys
from datetime import date,timedelta
import pandas as pd
import numpy as np
import math
import json
from decimal import *
from itertools import groupby
from boto3 import client
import boto3
import pandas as pd
import s3fs
from kafka import KafkaProducer
import os,pandas as pd, numpy as np,json,math,random,time,re,datetime
from pandas.io.json import json_normalize
from scipy import stats

REGION = 'ap-south-1'

def scientific_per(x):
    yy=float("{:.4f}".format(float(x)*100))
    return yy

def reward_sup1(data_l ,conv_data, test_weight):
    """
    Just a secondary function to reward_func1
    A function to calculate reward by taking inputs a Data length array,
    a Conv_data matrix a test_range arrray and a test_weight array
    """
    # conv_l and data_l is the lenght matrix of all the elements in conv_data and data respectively
    conv_l = [len(conv_data[i]) for i in range(len(conv_data))]
    #per_conv is the percentage converted    
    per_conv = []
    for i in range(len(data_l)):
        per = 0
        if data_l[i] !=0:
            per = conv_l[i]*100/data_l[i]
        per_conv = np.append(per_conv, [per])
        
    #padding the conv_data so that conv percentage is same for all paths  
    per_conv_max = max(per_conv)
    for i in range(len(conv_data)):
        if data_l[i] ==0:
            a = 1
        else:
            a =(per_conv_max - per_conv[i])*data_l[i]/100
            if a == float('Inf'):
                a=1
                
            a = int(a)
        conv_data[i] = np.append(conv_data[i], np.random.normal(0, 0, a))
        
    # R is the rank of conv_data_p 
    #calculating rank R
    merged = []
    for i in conv_data:
        merged = np.append(merged , i)
        
    R = [0 for i in range(len(merged))]
    k = [(merged[i], i) for i in range(len(merged))]
    k.sort(key=lambda x: x[0])
    (rank, n, i) = (1, 1, 0) 
    while i < len(merged): 
        j = i    
        while j < len(merged) - 1 and k[j][0] == k[j + 1][0]: 
            j += 1
        n = j - i + 1  
        for j in range(n):
            idx = k[i+j][1] 
            R[idx] = rank + (n - 1) * 0.5 
        rank += n 
        i += n
    
    # reward is the reward matrix in which every emelent is calculated as a sum of testweight of desired range multiplied by rank
    reward_mean = []
    k = 0
    for i in range(len(conv_data)):
        test_data = conv_data[i]
        reward_test = 0
        for j in test_data:
            reward_test = reward_test + R[k]*test_weight
            k += 1
        if len(test_data) !=0:    
            reward_test = reward_test/len(test_data)        
        reward_mean = np.append(reward_mean, reward_test)    
       
    return reward_mean


def rewardfunc1(data_l , conv_data ,test_range, test_weight):
    """
    A fuction to calculate Weighted Rank reward 
    This fuction splits the conversion data based on which test range the data lies
    
    """
    conv_data_n =[]
    n1 = len(test_range)
    for i in range(len(conv_data)):
        test_data = conv_data[i]
       
        a = c = []
        for j in test_data:
            if j <= test_range[0]:
                a = np.append(a,j)
                
            elif j > test_range[n1-1]:
                c = np.append(c,j) 
        if len(a)==0:
            a=[0]
        conv_data_n.append(a)
        
        for l in range(n1-1):
            lower_lim = test_range[l] 
            up_lim = test_range[l+1]
            b = []
            for j in test_data:
                if j > lower_lim and j <= up_lim:
                    b = np.append(b,j)
            if len(b) == 0:
                b =[0]
            conv_data_n.append(b)
            
        if len(c)==0:
            c=[0]
        conv_data_n.append(c)

#now conv_data _n is list of list which contains list of conversion data of various path in a paticular range in order of range then path        
    reward_mat = []
    for n in range(n1+1):
        conv_data0=[]
        for i in range(len(conv_data)):
            conv_data0.append(conv_data_n[n+ (n1+1)*i])

        reward0 = reward_sup1(data_l ,conv_data0, test_weight[n])
        reward_mat.append(reward0)
        
    reward =[] 
    for i in range(len(conv_data)):
        a=0
        for j in reward_mat:
            a += j[i]
        a = a/(n1+1)
        reward = np.append(reward, a)
         
    return reward
    

def rewardfunc2(data_l,conv_data):
    """
    ARPU reward function
    """
    reward = []
    for i in range(len(data_l)):
        if data_l[i] !=0:
            reward = np.append(reward,sum(conv_data[i]/data_l[i]))
        else:
            reward = np.append(reward,0)
    return reward


class Node:
    def __init__(self, d): 
        self.data = d 
        self.left = None
        self.right = None
    
    
def sortedArrayToBST(arr): 
    if len(arr)==0 : 
        return None
    mid = int((len(arr)) / 2)
    root = Node(arr[mid])
    root.left = sortedArrayToBST(arr[:mid])
    root.right = sortedArrayToBST(arr[mid+1:]) 
    return root 


def getCount(root,low,high):
    if root == None: 
        return 0
    
    if root.data < high and root.data >= low:  
        return (1 + getCount(root.left, low, high) + 
                    getCount(root.right, low, high))
        return getCount(root.right, low, high) 
   
    elif root.data < low:
        return getCount(root.right, low, high)
    
    else: 
        return getCount(root.left, low, high)
    
 
def count_(bst,lh):
    low = lh[0]
    high = lh[1]
    return getCount(bst,low,high)


def reward_func3(conv_data):
    """
    An rank biserial correlation reward function
    """
    count = [1 for i in range(len(conv_data))] 
    for l in range(len(conv_data)):
        arr_h = np.sort(conv_data[l])
        arr_l =  np.append([0], arr_h)
        lh = [[arr_l[k],arr_h[k]] for k in range(len(arr_h))]
        dot_arr1 = [.00001 for i in range(len(arr_h))]
        for j in range(len(conv_data)):
            if l  != j:
                arr = np.sort(conv_data[j])
                bst = sortedArrayToBST(arr)
                dot_arr01 = map(lambda x: count_(bst,x),lh)
                dot_arr01 = np.fromiter(dot_arr01, dtype=np.float64)
                dot_arr01 = np.cumsum(dot_arr01) 
                dot_arr1 = np.multiply(dot_arr1,dot_arr01)
        
        count[l] = sum(dot_arr1)
    reward = [t/sum(count) for t in count]
    return reward


def reward_func4(conv_data,data_l):
    """
    reward_func3 with padding
    """
    conv_l = []
    for i in range(len(data_l)):
        conv_l = np.append(conv_l, len(conv_data[i]))
        
    #per_conv is the percentage converted    
    per_conv = []
    for i in range(len(data_l)):
        per = 0
        if data_l[i] !=0:
            per = conv_l[i]*100/data_l[i]
        a = [per]
        per_conv = np.append(per_conv, a)
        
    #padding the conv_data so that conv per is same
    per_conv_max = max(per_conv)
    for i in range(len(conv_data)):
        if data_l[i] ==0:
            a = 1
        else:
            a =(per_conv_max - per_conv[i])*data_l[i]/100
            if a == float('Inf'):
                a=1
                
            a = int(a)
        conv_data[i] = np.append(conv_data[i], np.random.normal(0, 0, a))
        
    reward = reward_func3(conv_data)
    return reward


def thompsons(numbers_of_rewards_1, numbers_of_rewards_0, N,d ):
    winning_path=[]
    for n in range(N):
        ad =  0
        max_random = 0
        for i in range(d) :   
            random_beta =  np.random.beta(numbers_of_rewards_1[i] +1,
                                          numbers_of_rewards_0[i]+1,1) 
            if random_beta > max_random: 
                max_random =  random_beta
                ad = i
        winning_path = np.append(winning_path,ad)

    s =  [float(winning_path.tolist().count(i))/N for i in range(d)] 
    return s 

 
def prob_mab(file2,num_paths,reward_func,test_range,
                    test_weight,algorithm,time_step,value_par):
    """
    A function that returns probabilty of each path based on various parameter     
    """
    conv_data = []
    data_l = [0 for i in range(num_paths)]
    for p in range(num_paths):
        conv_data.append([])
    
    for i in range(len(file2['path'].values)):
        if file2['revenue'].values[i] !=0:
            conv_data[file2['path'].values[i]] = np.append(conv_data[file2['path'].values[i]],file2['revenue'].values[i])
        data_l[file2['path'].values[i]] += 1
    
    print(conv_data)
    print(len(conv_data))
    print(data_l)   
    #reward calculations based on reward function
    # if reward_func == 'wr':
    #     reward = rewardfunc1(data_l,conv_data,test_range,test_weight)
    
    # elif reward_func == 'cwr':
    #     test_range1 = []
    #     conv_data_merged = []
    #     for i in range(num_paths):
    #         conv_data_merged = np.append(conv_data_merged,conv_data[i])
    #     conv_data_sorted = np.sort(conv_data_merged)
    #     for i in test_range:
    #         test_range1 = np.append(test_range1,conv_data_sorted[int(i*len(conv_data_sorted))])
        
    #     reward = rewardfunc1(data_l,conv_data,test_range1,test_weight)
        
    if reward_func == 'arpu':
        reward = rewardfunc2(data_l,conv_data)
        
    elif reward_func == 'bi':
        reward = []
        for i in range(len(data_l)):
            reward = np.append(reward,len(conv_data[i])/data_l[i])
        print(reward)
    
    elif reward_func == 'rbc':
        print(conv_data)
        reward = reward_func3(conv_data)
        alfa = [int(reward[i]*len(conv_data[i])) for i in range(len(reward))]
        beta = [int((1-reward[i])*len(conv_data[i])) for i in range(len(reward))]
        N = 100000
        d =  len(alfa)
        prob = thompsons(alfa,beta,N,d)
        reward = prob
        print(reward)
        print(alfa)
        print(beta)
    
    elif reward_func == 'rbc_p':
        reward = reward_func4(conv_data,data_l)  
        alfa = [int(reward[i]*len(conv_data[i])) for i in range(len(reward))]
        beta = [int((1-reward[i])*len(conv_data[i])) for i in range(len(reward))]
        N = 100000
        d =  len(alfa)
        prob = thompsons(alfa,beta,N,d)
        reward = prob
        
    else:
        print('not a valid reward function using ARPU')
        reward = rewardfunc2(data_l,conv_data)
    
    #calculation of probabiliy based on algorithm  
    if algorithm == 'ep_gr':
        prob = [value_par/num_paths for i in range(num_paths)]
        m = max(reward)
        ind =0
        for i in range(len(reward)):
            if reward[i]== m:
                ind = i
        prob[ind] += (1-value_par)
        
    elif algorithm == 'ep_gr_a':
        ep = value_par*np.log10(time_step + 3)/(time_step + 3)
        prob = [ep/num_paths for i in range(num_paths)]
        m = max(reward)
        ind =0
        for i in range(len(reward)):
            if reward[i]== m:
                ind = i
        prob[ind] += (1-ep)
        
    elif algorithm == 'sftmx':
        #scaling elements of reward function in range 0 to 1
        reward1 = [i/sum(reward) for i in reward]
        reward = reward1
        temp = value_par
        z = sum([math.exp(float(v) / temp) for v in reward])
        prob = [math.exp(float(v) / temp) / z for v in reward]
        
    elif algorithm == 'sftmx_a':
        reward1 = [i/sum(reward) for i in reward]
        reward = reward1
        temp = value_par*np.log10(time_step + 3)/(time_step + 3)
        z = sum([math.exp(float(v) / temp) for v in reward])
        prob = [math.exp(float(v) / temp) / z for v in reward]
        
    elif algorithm == 'thompsons':
        reward1 = [i/sum(reward) for i in reward]
        reward = reward1
        if reward_func != 'rbc':
            if reward_func != 'rbc_p':
                alfa =[int(len(conv_data[i])) for i in range(len(reward))]
                beta = [data_l - alfa for data_l, alfa in zip(data_l, alfa)]
                N = 100000
                d =  len(alfa)
                prob = thompsons(alfa,beta,N,d)
                reward = prob
            
        prob = reward
        conf = prob
      
    else:
        print('Not a valid algorithm using epsilon gready with annealing')
        ep = value_par*np.log10(time_step + 3)/(time_step + 3)
        prob = [ep/num_paths for i in range(num_paths)]
        m = max(reward)
        ind =0
        for i in range(len(reward)):
            if reward[i]== m:
                ind = i
        prob[ind] += (1-ep)
        
    if algorithm != 'thompsons':
        reward = reward_func3(conv_data)
        alfa = [int(reward[i]*len(conv_data[i])) for i in range(len(reward))]
        beta = [int((1-reward[i])*len(conv_data[i])) for i in range(len(reward))]
        N = 100000
        d =  len(alfa)
        conf = thompsons(alfa,beta,N,d)
      
      
    return prob,conf

def count_zero(x):
    return(len(x.revenue[x.revenue>0]))

def truncated_wilcox(x,y):
    conv_x=len(x[x>0])/len(x)
    conv_y=len(y[y>0])/len(y)
    if(conv_x>conv_y):
        zero_pad=(conv_x-conv_y)*len(y[y>0])
        y_new= list(y[y>0]) + [0]*round(zero_pad)
        x_new=list(x[x>0])
    else:
        zero_pad=(conv_y-conv_x)*len(x[x>0])
        x_new=list(x[x>0]) + [0]*round(zero_pad)
        y_new=list(y[y>0])
    wil_test=stats.mannwhitneyu(x_new,y_new, use_continuity=True, alternative=None)[1]
    return(wil_test)


def Mab_framework(file1_loc,file2_loc):
    """
    A function that takes input from the file and dumps probability to the file
    file1_loc is the location to json file
    file2_loc is the location to csv file
    """
    from boto3 import client
    import boto3
    import pandas as pd
    import s3fs
    import re 
    conn = client('s3', region_name=REGION)  # again assumes boto.cfg setup, assume AWS S3 
    file2_gz=list(filter(lambda k: 'gz' in k, [key['Key'] for key in conn.list_objects(Bucket='g24x7.mab-data',Prefix=file2_loc)['Contents']]))
    file1_txt=list(filter(lambda k: 'init.txt' in k,[key['Key'] for key in conn.list_objects(Bucket='g24x7.mab-data',Prefix=file1_loc)['Contents']]))
    file2_gz_loc=['s3://g24x7.mab-data/' + i for i in file2_gz]
    fs = s3fs.S3FileSystem(client_kwargs={"region_name": REGION})
    li = []
    from datetime import datetime
    for filename in file2_gz_loc:
        with fs.open(filename) as f:
            df = pd.read_csv(f, compression='gzip',names=['user_id','path','revenue'])
            text=filename
            match = re.search(r'\d{4}-\d{2}-\d{2}', text)
            date = datetime.strptime(match.group(), '%Y-%m-%d').date()
            df['date']=[date]*(df.shape[0])
        li.append(df)
    frame = pd.concat(li, axis=0, ignore_index=True)
    
    BUCKET = 'g24x7.mab-data'
    FILE_TO_READ = file1_txt[0]
    client = boto3.client('s3', region_name=REGION)
    result = client.get_object(Bucket=BUCKET, Key=FILE_TO_READ) 
    text = result["Body"].read().decode()
    file01=json.loads(text)
    num_paths = int(file01['num_paths'])
    per_matrix = file01['per_matrix']
    reward_func = file01['reward_func']
    test_range = [0,0]
    test_weight = [1,1,1]
    algorithm = file01['algorithms']
    value_par = file01['value_par']
    delta = datetime.now() - datetime.strptime(file01['start_date'],'%Y-%m-%d')
    time_step =  delta.days
    day_n=int(file01['dn_value'])
    s_date=datetime.strptime(file01['start_date'],'%Y-%m-%d')
    day_s= s_date+timedelta(days=(2*day_n +1))
    diff=max(frame.date)-min(frame.date)
    if(diff.days>int(day_n)+1):
        if(per_matrix == 'rev'):
            conv=frame.groupby('date',as_index=False).apply(count_zero)
            total=frame.groupby('date',as_index=False).agg('count')
            conv_true=sum(conv[0:day_n+1])
            total_true=sum(total.revenue[0:day_n+1])
            conv_ratio=conv_true/total_true
            mdf1=frame[frame.date <= day_s.date()]#<=
            mdftest=mdf1[mdf1.revenue>0]
            mdf2=frame[frame.date > day_s.date()]#>
            mdf3= mdf2[mdf2.revenue>0]
            date_range=list(set(mdf2.date))
            d_list=[]
            for d in date_range:
                ddt=mdf3[mdf3.date==d]
                v_dat=[]
                v_dat.append(mdftest.revenue)
                v_dat.append(ddt.revenue)
                rew_rbc=reward_func3(v_dat) 
                if rew_rbc[1]>0.5:
                    low_val=int(round((rew_rbc[1]-0.5)*len(ddt.revenue)/0.5))
                    high_val=0
                else:
                    high_val=int(round((0.5-rew_rbc[1])*len(ddt.revenue)/0.5))
                    low_val=0
                count_path=ddt.groupby('path',as_index=False).agg({'revenue':'count'})
                ratio_imp=count_path.revenue/len(ddt.revenue)
                imp_val=round(ratio_imp*max(low_val,high_val))
                path=[]
                imp=[]
                for x in range(len(imp_val)):
                    path.append([x]*int(imp_val[x]))
                    if low_val>0:
                        sd=[0.001]*(int(imp_val[x]))
                    if high_val>0:
                        sd=[10000000000]*int(imp_val[x])
                    imp.append(sd)
                    path_list = [item for sublist in path for item in sublist]
                    imp_list=[item for sublist in imp for item in sublist]
                    date=[d]*len(path_list)
                    ddt2 = pd.DataFrame(list(zip(path_list,imp_list,date)),columns =['path', 'revenue','date']) 
                d_list.append(ddt2)

            mdf4=pd.concat(d_list, axis=0, ignore_index=True)

            mdf5=pd.concat([mdf1[['path','revenue','date']],mdf2[['path','revenue','date']],mdf4], axis=0)

            #for d in date_range:
             #   d_test=mdf5[mdf5.date == d]
              #  d2_test=d_test.revenue[d_test.revenue>0]
              #  v_dat=[]
              #  v_dat.append(mdftest.revenue)
              #  v_dat.append(d2_test)
              #  rew_rbc=reward_func3(v_dat) 
              #  print(rew_rbc)


            lis = []
            for d in date_range:
                dft=mdf5[mdf5.date==d]
                convert=sum(dft.revenue>0)
                total_num=len(dft.revenue)
                convert_path= dft.groupby('path',as_index=False).apply(count_zero)
                total_path=dft.groupby('path',as_index=False).agg({'revenue':'count'})
                zi=convert/conv_ratio
                path_conv_normalised = zi/total_num
                zeros=round(((total_path.revenue*path_conv_normalised)-convert_path))
                path=[]
                zero=[]
                for x in range(len(convert_path)):
                    path.append([x]*int(zeros[x]))
                    zero.append([0]*int(zeros[x]))
                    path_list = [item for sublist in path for item in sublist]
                    zero_list=[item for sublist in zero for item in sublist]
                    date=[d]*len(path_list)
                    dft2 = pd.DataFrame(list(zip(path_list,zero_list,date)),columns =['path', 'revenue','date']) 
                lis.append(dft2)



            mdf6=pd.concat(lis, axis=0, ignore_index=True)
            mdf7=mdf5[mdf5.date > day_s.date()]
            mdf7=mdf7[mdf7.revenue>0]

            final_df=pd.concat([mdf1[['path','revenue','date']],mdf7[['path','revenue','date']],mdf6], axis=0)
            file2=final_df


        if(per_matrix == 'conv'):

            conv=frame.groupby('date',as_index=False).apply(count_zero)
            total=frame.groupby('date',as_index=False).agg('count')
            conv_true=sum(conv[0:day_n+1])
            total_true=sum(total.revenue[0:day_n+1])
            conv_ratio=conv_true/total_true
            mdf1=frame[frame.date <= day_s.date()]#<=
            mdftest=mdf1[mdf1.revenue>0]
            mdf2=frame[frame.date > day_s.date()]#>
            mdf3= mdf2[mdf2.revenue>0]
            date_range=list(set(mdf2.date))

            lis = []
            for d in date_range:
                dft=mdf2[mdf2.date==d]
                convert=sum(dft.revenue>0)
                total_num=len(dft.revenue)
                convert_path= dft.groupby('path',as_index=False).apply(count_zero)
                total_path=dft.groupby('path',as_index=False).agg({'revenue':'count'})
                zi=convert/conv_ratio
                path_conv_normalised = zi/total_num
                zeros=round(((total_path.revenue*path_conv_normalised)-convert_path))
                path=[]
                zero=[]
                for x in range(len(convert_path)):
                    path.append([x]*int(zeros[x]))
                    zero.append([0]*int(zeros[x]))
                    path_list = [item for sublist in path for item in sublist]
                    zero_list=[item for sublist in zero for item in sublist]
                    date=[d]*len(path_list)
                    dft2 = pd.DataFrame(list(zip(path_list,zero_list,date)),columns =['path', 'revenue','date']) 
                lis.append(dft2)



            mdf6=pd.concat(lis, axis=0, ignore_index=True)
            mdf7=mdf2[mdf2.revenue>0]

            final_df=pd.concat([mdf1[['path','revenue','date']],mdf7[['path','revenue','date']],mdf6], axis=0)
            file2=final_df
    else:
        file2=frame
  

    if per_matrix == 'rev' and reward_func == 'bi':
        print('Only conversion can be used as perfomance matrix for binomial reward, So using conversion')
    if per_matrix == 'conv' and reward_func != 'bi':
        print('Only revenue can be used as perfomance matrix for {} reward, So using revenue'.format(reward_func))
    if time_step > file01['dn_value']:
        prob,conf = prob_mab(file2,num_paths,reward_func,test_range,test_weight,algorithm,time_step,value_par)
        dict_object = dict(exp_id=file01['exp_id'],prob_various_paths = list(map(scientific_per,prob)),prob_actual_winning=list(map(scientific_per,conf)))
    else:
        print("The metric is not Dn mature")
        dict_object=dict(exp_id=file01['exp_id'],message = "The metric is not"+str(file01['dn_value'])+ "mature")  
    print(dict_object)     
    kafka_brokers=['eds-kafka-1.pdc.games24x7.com:9092','eds-kafka-2.pdc.games24x7.com:9092','eds-kafka-3.pdc.games24x7.com:9092']
    producer = KafkaProducer(bootstrap_servers=kafka_brokers,\
                         max_request_size=3048576,compression_type='gzip' \
                         ,acks='all',value_serializer=lambda v: json.dumps(v).encode('utf-8'),\
                         api_version=(2,0,0))
                         
    producer.send('mabExperimentTopic', value=dict_object)
    producer.flush()
    s3 = boto3.resource('s3', region_name=REGION)
    s3object = s3.Object(BUCKET, file1_loc+'MABresult/results.json')

    s3object.put(       
    Body=(bytes(json.dumps(dict_object).encode('UTF-8')))
    )


#Final file locaation
config_loc=sys.argv[1]
data_loc=sys.argv[2]
print(config_loc)
print(data_loc)
Mab_framework(config_loc,data_loc) 