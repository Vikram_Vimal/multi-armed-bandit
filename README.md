# Multi-armed Bandit

MAB.py is the base file with all the algorithms.

MAB_Kafka_deploy.py is the one to be used in production.

More on MAB:https://docs.google.com/document/d/1saU9votkXOR9liv5AJTxSPxf5yVKV2NQpUlLdxah8Sw/edit?usp=sharing
Input format of MAB:https://docs.google.com/document/d/10ZrWu3r03g5CV-A5x2MSTNwb9fEjH8AnZImeQbd8SFo/edit?usp=sharing