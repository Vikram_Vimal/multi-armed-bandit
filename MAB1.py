import pandas as pd
import numpy as np
import math
import json
from decimal import *

def reward_sup1(data_l ,conv_data, test_weight):
    """
    Just a secondary function to reward_func1
    A function to calculate reward by taking inputs a Data length array,
    a Conv_data matrix a test_range arrray and a test_weight array
    """
    # conv_l and data_l is the lenght matrix of all the elements in conv_data and data respectively
    conv_l = [len(conv_data[i]) for i in range(len(conv_data))]
    #per_conv is the percentage converted    
    per_conv = []
    for i in range(len(data_l)):
        per = 0
        if data_l[i] !=0:
            per = conv_l[i]*100/data_l[i]
        per_conv = np.append(per_conv, [per])
        
    #padding the conv_data so that conv percentage is same for all paths  
    per_conv_max = max(per_conv)
    for i in range(len(conv_data)):
        if data_l[i] ==0:
            a = 1
        else:
            a =(per_conv_max - per_conv[i])*data_l[i]/100
            if a == float('Inf'):
                a=1
                
            a = int(a)
        conv_data[i] = np.append(conv_data[i], np.random.normal(0, 0, a))
        
    # R is the rank of conv_data_p 
    #calculating rank R
    merged = []
    for i in conv_data:
        merged = np.append(merged , i)
        
    R = [0 for i in range(len(merged))]
    k = [(merged[i], i) for i in range(len(merged))]
    k.sort(key=lambda x: x[0])
    (rank, n, i) = (1, 1, 0) 
    while i < len(merged): 
        j = i    
        while j < len(merged) - 1 and k[j][0] == k[j + 1][0]: 
            j += 1
        n = j - i + 1  
        for j in range(n):
            idx = k[i+j][1] 
            R[idx] = rank + (n - 1) * 0.5 
        rank += n 
        i += n
    
    # reward is the reward matrix in which every emelent is calculated as a sum of testweight of desired range multiplied by rank
    reward_mean = []
    k = 0
    for i in range(len(conv_data)):
        test_data = conv_data[i]
        reward_test = 0
        for j in test_data:
            reward_test = reward_test + R[k]*test_weight
            k += 1
        if len(test_data) !=0:    
            reward_test = reward_test/len(test_data)        
        reward_mean = np.append(reward_mean, reward_test)    
       
    return reward_mean


def rewardfunc1(data_l , conv_data ,test_range, test_weight):
    """
    A fuction to calculate Weighted Rank reward 
    This fuction splits the conversion data based on which test range the data lies
    
    """
    conv_data_n =[]
    n1 = len(test_range)
    for i in range(len(conv_data)):
        test_data = conv_data[i]
       
        a = c = []
        for j in test_data:
            if j <= test_range[0]:
                a = np.append(a,j)
                
            elif j > test_range[n1-1]:
                c = np.append(c,j) 
        if len(a)==0:
            a=[0]
        conv_data_n.append(a)
        
        for l in range(n1-1):
            lower_lim = test_range[l] 
            up_lim = test_range[l+1]
            b = []
            for j in test_data:
                if j > lower_lim and j <= up_lim:
                    b = np.append(b,j)
            if len(b) == 0:
                b =[0]
            conv_data_n.append(b)
            
        if len(c)==0:
            c=[0]
        conv_data_n.append(c)

#now conv_data _n is list of list which contains list of conversion data of various path in a paticular range in order of range then path        
    reward_mat = []
    for n in range(n1+1):
        conv_data0=[]
        for i in range(len(conv_data)):
            conv_data0.append(conv_data_n[n+ (n1+1)*i])

        reward0 = reward_sup1(data_l ,conv_data0, test_weight[n])
        reward_mat.append(reward0)
        
    reward =[] 
    for i in range(len(conv_data)):
        a=0
        for j in reward_mat:
            a += j[i]
        a = a/(n1+1)
        reward = np.append(reward, a)
         
    return reward
    

def rewardfunc2(data_l,conv_data):
    """
    ARPU reward function
    """
    reward = []
    for i in range(len(data_l)):
        if data_l[i] !=0:
            reward = np.append(reward,sum(conv_data[i]/data_l[i]))
        else:
            reward = np.append(reward,0)
    return reward


class Node:
    def __init__(self, d): 
        self.data = d 
        self.left = None
        self.right = None
    
    
def sortedArrayToBST(arr): 
    if len(arr)==0 : 
        return None
    mid = int((len(arr)) / 2)
    root = Node(arr[mid])
    root.left = sortedArrayToBST(arr[:mid])
    root.right = sortedArrayToBST(arr[mid+1:]) 
    return root 


def getCount(root,low,high):
    if root == None: 
        return 0
    
    if root.data < high and root.data >= low:  
        return (1 + getCount(root.left, low, high) + 
                    getCount(root.right, low, high))
        return getCount(root.right, low, high) 
   
    elif root.data < low:
        return getCount(root.right, low, high)
    
    else: 
        return getCount(root.left, low, high)
    
 
def count_(bst,lh):
    low = lh[0]
    high = lh[1]
    return getCount(bst,low,high)


def reward_func3(conv_data):
    """
    An rank biserial correlation reward function
    """
    getcontext().prec = 60
    count = [1 for i in range(len(conv_data))] 
    for i in range(len(conv_data)):
        arr_h = np.sort(conv_data[i])
        arr_l =  np.append([0], arr_h)
        lh = [[arr_l[k],arr_h[k]] for k in range(len(arr_h))]
        dot_arr1 = [.00001 for i in range(len(arr_h))]
        for j in range(len(conv_data)):
            if i  != j:
                arr = np.sort(conv_data[j])
                bst = sortedArrayToBST(arr)
                dot_arr01 = map(lambda x: count_(bst,x),lh)
                dot_arr01 = np.fromiter(dot_arr01, dtype=np.float64)
                dot_arr01 = np.cumsum(dot_arr01) 
                dot_arr1 = np.multiply(dot_arr1,dot_arr01)
        count[i] = Decimal(sum(dot_arr1))
     
    reward = [i/sum(count) for i in count]
    print(reward)
    return reward


def reward_func4(conv_data,data_l):
    """
    reward_func3 with padding
    """
    conv_l = []
    for i in range(len(data_l)):
        conv_l = np.append(conv_l, len(conv_data[i]))
        
    #per_conv is the percentage converted    
    per_conv = []
    for i in range(len(data_l)):
        per = 0
        if data_l[i] !=0:
            per = conv_l[i]*100/data_l[i]
        a = [per]
        per_conv = np.append(per_conv, a)
        
    #padding the conv_data so that conv per is same
    per_conv_max = max(per_conv)
    for i in range(len(conv_data)):
        if data_l[i] ==0:
            a = 1
        else:
            a =(per_conv_max - per_conv[i])*data_l[i]/100
            if a == float('Inf'):
                a=1
                
            a = int(a)
        conv_data[i] = np.append(conv_data[i], np.random.normal(0, 0, a))
        
    reward = reward_func3(conv_data)
    return reward


def thompsons(numbers_of_rewards_1, numbers_of_rewards_0, N,d ):
    winning_path=[]
    for n in range(N):
        ad =  0
        max_random = 0
        for i in range(d) :   
            random_beta =  np.random.beta(numbers_of_rewards_1[i] +1,
                                          numbers_of_rewards_0[i]+1,1) 
            if random_beta > max_random: 
                max_random =  random_beta
                ad = i
        winning_path = np.append(winning_path,ad)
    
    s = [sum(winning_path==i)/N for i in range(d)]
    return s 

 
def prob_mab(file2,num_paths,reward_func,test_range,
                    test_weight,algorithm,time_step,value_par):
    """
    A function that returns probabilty of each path based on various parameter     
    """
    conv_data = []
    data_l = [0 for i in range(num_paths)]
    for p in range(num_paths):
        conv_data.append([])
    
    for i in range(len(file2['path'].values)):
        if file2['revenue'].values[i] !=0:
            conv_data[file2['path'].values[i]] = np.append(conv_data[file2['path'].values[i]],file2['revenue'].values[i])
        data_l[file2['path'].values[i]] += 1
    
    print(conv_data)
    print(data_l)   
    #reward calculations based on reward function
    if reward_func == 'wr':
        reward = rewardfunc1(data_l,conv_data,test_range,test_weight)
    
    elif reward_func == 'cwr':
        test_range1 = []
        conv_data_merged = []
        for i in range(num_paths):
            conv_data_merged = np.append(conv_data_merged,conv_data[i])
        conv_data_sorted = np.sort(conv_data_merged)
        for i in test_range:
            test_range1 = np.append(test_range1,conv_data_sorted[int(i*len(conv_data_sorted))])
        
        reward = rewardfunc1(data_l,conv_data,test_range1,test_weight)
        
    elif reward_func == 'arpu':
        reward = rewardfunc2(data_l,conv_data)
        
    elif reward_func == 'bi':
        reward = []
        for i in range(len(data_l)):
            reward = np.append(reward,len(conv_data[i])/data_l[i])
        print(reward)
    
    elif reward_func == 'rbc':
        reward = reward_func3(conv_data)
        alfa = [int(reward[i]*len(conv_data[i])) for i in range(len(reward))]
        beta = [int((1-reward[i])*len(conv_data[i])) for i in range(len(reward))]
        N = 100000
        d =  len(alfa)
        prob = thompsons(alfa,beta,N,d)
        reward = prob
        print(reward)
        print(alfa)
        print(beta)
    
    elif reward_func == 'rbc_p':
        reward = reward_func4(conv_data,data_l)  
        alfa = [int(reward[i]*len(conv_data[i])) for i in range(len(reward))]
        beta = [int((1-reward[i])*len(conv_data[i])) for i in range(len(reward))]
        N = 100000
        d =  len(alfa)
        prob = thompsons(alfa,beta,N,d)
        reward = prob
        
    
    else:
        print('not a valid reward function using ARPU')
        reward = rewardfunc2(data_l,conv_data)
    
    #calculation of probabiliy based on algorithm  
    if algorithm == 'ep_gr':
        prob = [value_par/num_paths for i in range(num_paths)]
        m = max(reward)
        ind =0
        for i in range(len(reward)):
            if reward[i]== m:
                ind = i
        prob[ind] += (1-value_par)
        
    elif algorithm == 'ep_gr_a':
        ep = value_par*np.log10(time_step + 3)/(time_step + 3)
        prob = [ep/num_paths for i in range(num_paths)]
        m = max(reward)
        ind =0
        for i in range(len(reward)):
            if reward[i]== m:
                ind = i
        prob[ind] += (1-ep)
        
    elif algorithm == 'sftmx':
        #scaling elements of reward function in range 0 to 1
        reward1 = [i/sum(reward) for i in reward]
        reward = reward1
        temp = value_par
        z = sum([math.exp(float(v) / temp) for v in reward])
        prob = [math.exp(float(v) / temp) / z for v in reward]
        
    elif algorithm == 'sftmx_a':
        reward1 = [i/sum(reward) for i in reward]
        reward = reward1
        temp = value_par*np.log10(time_step + 3)/(time_step + 3)
        z = sum([math.exp(float(v) / temp) for v in reward])
        prob = [math.exp(float(v) / temp) / z for v in reward]
        
    elif algorithm == 'thompsons':
        reward1 = [i/sum(reward) for i in reward]
        reward = reward1
        if reward_func != 'rbc':
            if reward_func != 'rbc_p':
                alfa =[int(len(conv_data[i])) for i in range(len(reward))]
                beta = [data_l - alfa for data_l, alfa in zip(data_l, alfa)]
                N = 100000
                d =  len(alfa)
                prob = thompsons(alfa,beta,N,d)
                reward = prob
            
        prob = reward
        conf = prob
      
    else:
        print('Not a valid algorithm using epsilon gready with annealing')
        ep = value_par*np.log10(time_step + 3)/(time_step + 3)
        prob = [ep/num_paths for i in range(num_paths)]
        m = max(reward)
        ind =0
        for i in range(len(reward)):
            if reward[i]== m:
                ind = i
        prob[ind] += (1-ep)
        
    if algorithm != 'thompsons':
        reward = reward_func3(conv_data)
        alfa = [int(reward[i]*len(conv_data[i])) for i in range(len(reward))]
        beta = [int((1-reward[i])*len(conv_data[i])) for i in range(len(reward))]
        N = 100000
        d =  len(alfa)
        conf = thompsons(alfa,beta,N,d)
      
      
    return prob,conf


def Mab_framework(file1_loc,file2_loc,file3_loc):
    """
    A function that takes input from the file and dumps probability to the file
    file1_loc is the location to json file
    file2_loc is the location to csv file
    """
    with open(file1_loc) as json_file:  
        file01 = json.load(json_file)
    file2 = pd.read_csv(file2_loc)
    num_paths = int(file01['num_paths'])
    per_matrix = file01['per_matrix']
    reward_func = file01['reward_func']
    test_range = file01['test_range']
    test_weight = file01['test_weight']
    algorithm = file01['algorithm']
    value_par = file01['value_par']
    time_step = file01['time_step'] + 1
    if per_matrix == 'rev' and reward_func == 'bi':
        print('Only conversion can be used as perfomance matrix for binomial reward, So using conversion')
    if per_matrix == 'conv' and reward_func != 'bi':
        print('Only revenue can be used as perfomance matrix for {} reward, So using revenue'.format(reward_func))
        
    prob,conf = prob_mab(file2,num_paths,reward_func,test_range,
                    test_weight,algorithm,time_step,value_par)
    
    file01['time_step'] = time_step
    dict_object = dict(prob_various_paths = prob, prob_actual_winning= conf)
    file03 = json.dumps(dict_object)
    with open(file1_loc, 'w') as outfile:  
        json.dump(file01, outfile)
    with open(file3_loc, 'w') as outfile:  
        json.dump(file03, outfile)



Mab_framework('file1.txt','file2.csv','results.txt')